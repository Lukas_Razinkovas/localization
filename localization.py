from __future__ import print_function
import random
import itertools
import pprint
import numpy as np
from collections import Counter
import matplotlib.pyplot as plt
import time

pp = pprint.PrettyPrinter()


def memo(f):
    """ Memoization wrapper. """
    def _f(*args):
        if args not in _f.cache:
            _f.cache[args] = f(*args)
        return _f.cache[args]
    _f.cache = {}
    _f.__name__ = f.__name__
    return _f


##################################
# MODEL DESCRIPTION AND PARAMETERS

DIRS = "NESW"               # Compass directions
VDIRS = "↑→↓←"              # Compass directions
WALL, PATH = 'X', '^'       # Evidence/ measurement values
NONE, MARKS = ' ', "Oo."    # Density/ probability markers
noise = 0.1                 # Probability of equipment failure
confidence = 0.85           # Confidence of final (localized) state
SHOW = False                # Toggle to show localization process

# Map of the world
"""
X X X X X X X X X X X X
6 X X X       X X X X X
5 X X X   X   X X X X X
4                     X
3 X X X   X   X X X   X
2                   X X
1 X X X   X   X X X X X
0 X X X     X X X X X X
X 0 1 2 3 4 5 6 7 8 9 X
"""

# List of coridors
world = ((3, 0), (4, 0), (3, 1), (5, 1)) + tuple([(i, 2) for i in range(9)]) + \
        ((3, 3), (5, 3), (9, 3)) + tuple([(i, 4) for i in range(10)]) + \
        ((3, 5), (5, 5), (3, 6), (4, 6), (5, 6))


def draw_world(marks=None, world=world, state=None):
    if marks is None:
        marks = {}

    if state is not None:
        marks[state[0]] = VDIRS[DIRS.index(state[1])]

    """ Draw world map (with optional marks for each state). """
    X, Y = max([x for (x, y) in world]), max([y for (x, y) in world])
    print(WALL, ' '.join([WALL for x in range(X+1)]), WALL)
    for y in range(Y, -1, -1):
        print(y % 10, end=" ")
        for x in range(0, X+2, 1):
            if (x, y) not in world:
                print(WALL, end=" ")
            elif (x, y) not in marks:
                print(NONE, end=" ")
            else:
                print(marks[(x, y)], end=" ")
        print()

    print(WALL, ' '.join([str(x % 10) for x in range(X+1)]), WALL)
    print()


@memo
def walls(state, world=world):
    """Return evidence of world state."""
    (loc, d) = state
    (x, y) = loc
    w = (
        [WALL if l not in world else PATH
         for l in [(x, y+1), (x+1, y), (x, y-1), (x-1, y)]])
    c = DIRS.index(d)
    return ''.join(w[c:]+w[:c])


@memo
def states(world=world):
    """Set of all possible states."""
    return tuple(itertools.product(world, DIRS))


def forward_loc(cur_state):
    """Returns new location after step forward."""
    loc, d = cur_state
    forward = {
        "N": np.array([0, 1]),
        "E": np.array([1, 0]),
        "S": np.array([0, -1]),
        "W": np.array([-1, 0])
    }

    return (tuple(np.array(loc) + forward[d]), d)


def possible_moves(cur_state):
    loc, d = cur_state
    moves = []
    f = forward_loc(cur_state)
    if f[0] in world:
        moves.append(f)

    left = DIRS[DIRS.index(d) - 1]
    try:
        right = DIRS[DIRS.index(d) + 1]
    except IndexError:
        right = DIRS[0]

    moves.extend([(loc, left), (loc, right)])
    return moves


##############################
#   HIDDEN MARKOV MODEL
##############################

def init_distribution(world=world):
    return np.array([1/len(states())] * len(states()))


def trans_prob(cur_state, new_state):
    if cur_state == new_state:
        return noise

    moves = possible_moves(cur_state)
    prob = (1-noise)/len(moves)

    if new_state in moves:
        return prob

    return 0


def make_markov_matrix():
    matrix = []
    for to in states():
        row = []
        for fr in states():
            row.append(trans_prob(fr, to))

        matrix.append(row)

    return np.array(matrix)

markov_matrix = make_markov_matrix()


@memo
def evidence_prob(state_nr, _map):
    """If I know state, what is the probability of map
    P(evidence|position)
    """
    if _map == walls(states()[state_nr]):
        return 1 - noise
    return noise


def cause_prob(_map, state_nr, val):
    """Bayes rule applied."""
    p = evidence_prob(state_nr, _map)*val
    return p


def forward(distr, evidence):
    distr = np.dot(markov_matrix, distr)

    for state_nr, val in enumerate(distr):
        distr[state_nr] = cause_prob(evidence, state_nr, val)

    distr = distr/sum(distr)

    return distr


def get_posibility_map(d):
    """For visual representation."""
    _map = {}
    for nr, (loc, _) in enumerate(states()):

        if _map.get(loc, 0) < d[nr]:
            _map[loc] = d[nr]

    for i in _map:
        tag = " "
        if _map[i] > 0.01:
            tag = "."
        if _map[i] > 0.1:
            tag = "o"
        if _map[i] > 0.5:
            tag = "O"
        _map[i] = tag

    return _map

############
# AGENT CODE
############


# Write code needed for agent localization
def localize(f_act, f_observe, other=None):
    """ Localization function of an agent. """
    distr = init_distribution(world)
    evidence = f_observe()
    distr = forward(distr, evidence)
    # draw_world(state=glob_state)
    # draw_world(get_posibility_map(distr))

    while distr.max() < confidence:
        act("RANDOM")
        evidence = f_observe()
        distr = forward(distr, evidence)
        # draw_world(state=glob_state)
        # draw_world(get_posibility_map(distr))
        # time.sleep(1)

    # draw_world(state=glob_state)
    # draw_world(get_posibility_map(distr))
    state = states()[distr.argmax()]
    return (state, max(distr))


######################
# AGENT TEST FRAMEWORK

# Current state of agent
glob_state = None


def observe():
    """ Return measurement results to agent. """
    global glob_state

    # Measurement with noise
    glitch = ''.join([WALL if random.random() >= 0.5 else PATH
                      for _ in range(len(DIRS))])
    evidence = walls(glob_state) if random.random() >= noise else glitch
    if SHOW:
        print('### Evidence:', evidence)

    return evidence


def act(action):
    """ Perform action on agent. """
    act.steps += 1
    global glob_state

    if SHOW:
        print('### Action:', action, glob_state, end="")

    # Action with control noise

    if random.random() >= noise:
        glob_state = random.choice(possible_moves(glob_state))

    if SHOW:
        print('->', glob_state)


def run_agent_run(start=None, n=1, world=world, show_work=False):
    """ Localization test framework. """
    global glob_state, SHOW
    SHOW = show_work
    tstep, wrong, t_tot = 0.0, 0.0, 0.0
    for i in range(n):
        act.steps = 0

        # Set start state randomly if not provided
        glob_state = (start if start is not None else
                      (random.choice(world), random.choice(DIRS)))

        print('### Start state:', glob_state)

        # Let agent localize itself
        t_s = time.clock()
        (state, p) = localize(act, observe)
        t_tot += time.clock() - t_s

        # Compare results with reality
        print('### Final state:', glob_state)
        print('--- Agent state:', state, ('%.3f' % p), end="")
        if (str(state) != str(glob_state)):
            wrong += 1
            print('(WRONG)')
        else:
            print()

        tstep += act.steps

    # Print cumulative statistics
    print('\nSTATS:', ('(%4d runs)' % n if n > 1 else ''))
    print(tstep/n, 'steps/run, ', (n-wrong)/n, 'accuracy, ', end="")
    print('%.3f' % (t_tot/n), 's/run\n')

# Test example
run_agent_run(n=200)

# run_agent_run(((8, 4), 'N'), 1)
# Statistics run
# run_agent_run(None, n=100, show_work=False)
